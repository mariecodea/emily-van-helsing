
/* 
 * File:   Player.cpp
 * Autora: Maria Emilia Corbetta
 */

#include "Player.h"

//******************************************************************************
//CONTRUCTOR Y DESTRUCTOR DE LA CLASE
//******************************************************************************
//contructor

Player::Player() {
    //instanciamos los objetos sprite y textura
    _texturaMira = new sf::Texture;
    _spriteMira = new sf::Sprite;

    //Cargamos las texturas y  ponemos texturas sprites
    _texturaMira->loadFromFile("Sprites/mira-sola-33x34.png");
    _spriteMira->setTexture(*_texturaMira);

    //establecer el centro de la imagen Mira, sirve para posicionar el mouse en el medio
    _spriteMira->setOrigin(_spriteMira->getLocalBounds().width / 2.0f, _spriteMira->getLocalBounds().height / 2.0f);

    coords.x = 0.f;
    coords.y = 0.f;
    puntos = 0;
    vidas = 3;

}
//destructor

Player::~Player() {
    delete(_spriteMira);
    delete(_texturaMira);
}

//******************************************************************************
//METODO SETRESTARVIDAS (setter)  resta vidas
//pone/setea el valor de vidas
//va restando  de a uno cada vez que se la llama
//se llama en juego cuando un enemigo nos pega o cuando matamos a un rehen
//******************************************************************************

void Player::SetRestarVida() {
    if (vidas <= 0)return;
    else {
        vidas--;
    }
}
//******************************************************************************
//METODO GETVIDAS (getter)
//devuleve el valor entero de vidas, se llama en  la clase Juego en el metodo actualizar()
//actualiza el valor de vidas en la UI ,sirve para dibujar los corazones (ui)
// tambien sirve para saber si hay cero vida y dar por perdido el juego
//******************************************************************************

int Player::GetVidas() {
    return vidas;
}

//******************************************************************************
//METODO SETSUMARPUNTOS (setter) suma puntos
//setea/pone un valor a puntos 
//va sumando de a uno cada vez que se llama a este metodo
//se llama a este metodo en juego cuando se le acierta el disparo a un enemigo
//******************************************************************************

void Player::SetSumarPuntos() {
    puntos++;
}

//********************************************************************************
//METODO GETPUNTOS   (getter)
//devuleve el valor de puntos, se llama en  la clase Juego en el metodo actualizar()
//actualiza el valor de puntos en la UI ,sirve para mostrar los puntos en la UI
//recordar que devuleve un entero, la Ui necesia convertir este entero a string
//**********************************************************************************

int Player::GetPuntos() {
    return puntos;
}

//******************************************************************************
//METODO SETCOORDENADSMOUSE   (setter)
//setea/pone las coordenadas que captura del evento del mouse 
//para eso guarda en el vector coords las coordenadas que recibe de x e y 
//se necesita para  poisionar la mira
//******************************************************************************

void Player::SetCoordenadasMouse(float x, float y) {
    coords.x = x;
    coords.y = y;
}

//******************************************************************************
//METODO GETCOORDENADASMIRA   (getter)
//retorna un vector con las coordenadas X e Y del cursor
//se usa para saber la posicion actual de la mira y comprar si colisiona 
//con  la posicion de un personaje
//*******************************************************************************

sf::Vector2f Player::GetCoordenadasMira() {
    return coords;
}

//******************************************************************************
//METODO PARA POSICIONAR LA MIRA
//posiciona la mira usando el vector coords (contiene la pos x e y de la mira)
//******************************************************************************

void Player::PosicionarMira() {
    _spriteMira->setPosition(coords);
}

//******************************************************************************
//METODO PARA ACTUALIZAR 
// actualiza la posicion de la mira
//******************************************************************************

void Player::Actualizar() {
    PosicionarMira();
}

//******************************************************************************
//METODO PARA DIBUJAR
//dibuja la mira
//******************************************************************************

void Player::Dibujar(sf::RenderWindow* _window) {
    _window->draw(*_spriteMira);
}



