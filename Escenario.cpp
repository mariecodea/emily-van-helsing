
/* 
 * File:   Escenario.cpp
 * Autora: Maria Emilia Corbetta
 
 */

#include "Escenario.h"

//*********************************************
//CONTRUCTOR Y DESTRUCTOR DE LA CLASE
//*********************************************
//constructor

Escenario::Escenario() {

    //instanciamos los objetos sprite y textura
    _texturaFondoCasa = new sf::Texture;
    _spriteFondoCasa = new sf::Sprite;

    //Cargamos las texturas y  ponemos texturas sprites
    _texturaFondoCasa->loadFromFile("Sprites/vanhelsing-background-buho-Casa-940X540.png");
    _spriteFondoCasa->setTexture(*_texturaFondoCasa);
}

//destructor

Escenario::~Escenario() {
    delete(_spriteFondoCasa);
    delete(_texturaFondoCasa);
}

//*********************************************
//METODO PARA DIBUJAR EL FONDO
//*********************************************

void Escenario::Dibujar(sf::RenderWindow* _window) {
    _window->draw(*_spriteFondoCasa);
}


