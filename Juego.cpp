
/* 
 * Archivo:   Juego.cpp
 * Autora: Maria Emilia Corbetta
 * 
 */

#include "Juego.h"

//******************************************
//CONTRUCTOR Y DESTRUCTOR DE LA CLASE
//******************************************
//CONSTRUCTOR
//////

Juego::Juego() {

    _window = new sf::RenderWindow(sf::VideoMode(940, 540), "Emily Van Helsing Game");
    _window->setFramerateLimit(60);
    _window->setMouseCursorVisible(false);

    //instanciamos los objetos
    _escenario = new Escenario;

    for (int i = 0; i < 5; i++) {
        _ventanasArr[i] = new Ventanas;
        _ventanasArr[i]->SetPosicionAberturas(i);
    }

    for (int i = 0; i < 5; i++) {
        _personajesVentArr[i] = new Personajes;
        _personajesVentArr[i]->SetPosicionPersonajes(i);
    }

    _puerta = new Puerta;
    _personajePuerta = new Personajes;
    _personajePuerta->SetPosicionPersonajes(5);
    _playerEmily = new Player;
    _ui = new Ui;
    _gameOver = new GameOver;
    _efectos = new Efectos;
    _sonido = new Sonido;

    //boleanas, le asignamos un valor inicial
    _estaDisparando = false;
    _juegoPerdido = false;

}
///////////////////////
//DESTRUCTOR

Juego::~Juego() {

    delete(_sonido);
    delete(_efectos);
    delete(_gameOver);
    delete(_ui);
    delete(_playerEmily);
    delete(_puerta);
    delete(_personajePuerta);
    for (int i = 0; i < 5; i++) {
        delete(_personajesVentArr[i]);
    }
    for (int i = 0; i < 5; i++) {
        delete(_ventanasArr[i]);
    }
    delete(_escenario);
    delete(_window);

}
//***************************************************************************************
//METODO PUBLICO INICIARJUEGO()
//Este metodo es el bucle principal del juego
//aqui se procesa todo lo que ocurran en este juego mientras la ventana este abierta
// procesa eventos, actualiza y dibuja
//****************************************************************************************

void Juego::IniciarJuego() {

    while (_window->isOpen()) {
        ProcesarEventos();
        Actualizar();
        Dibujar(_window);
    }
}
//*****************************************
//METODO PARA PROCESAR EVENTOS
//este metodo sirve para detectar eventos
//******************************************

void Juego::ProcesarEventos() {
    sf::Event event;

    while (_window->pollEvent(event)) {

        switch (event.type) {
                //caso si la ventana se cierra
            case sf::Event::Closed:
                _window->close();
                break;

                // movimiento del mouse
            case sf::Event::MouseMoved:
                _playerEmily->SetCoordenadasMouse(event.mouseMove.x, event.mouseMove.y);
                break;

            case sf::Event::MouseButtonPressed:
                if (event.mouseButton.button == sf::Mouse::Left) {
                    _estaDisparando = true;
                }
                break;
        }

    }
}
//*****************************************
//METODO ACTUALIZAR
//actualiza toda la logia del juego
//******************************************

void Juego::Actualizar() {
    _elapsed = _reloj.restart();

    if (!_juegoPerdido) {
        _sonido->SetMusicaInGame(true);
        //*Actualiza la logica, estados de todos los objetos
        _playerEmily->Actualizar();
        _puerta->Actualizar(_elapsed);
        _personajePuerta->Actualizar(_elapsed);
        for (int i = 0; i < 5; i++) {
            _ventanasArr[i]->Actualizar(_elapsed);
            _personajesVentArr[i]->Actualizar(_elapsed);
        }
        _efectos->Actualizar(_elapsed);

        // logica que activa o desctiva un personaje si el estado de la abertura esta abierto o cerrado
        GestionPersonajes(_personajePuerta, _puerta);
        for (int i = 0; i < 5; i++) {
            GestionPersonajes(_personajesVentArr[i], _ventanasArr[i]);
        }

        //*logica para detectar ataques por puerta y ventanas
        DetectarAtaque(_personajePuerta, _puerta);
        for (int i = 0; i < 5; i++) {
            DetectarAtaque(_personajesVentArr[i], _ventanasArr[i]);
        }

        //*logica para el disparo
        if (_estaDisparando) {
            for (int i = 0; i < 5; i++) {
                DetectarDisparo(_personajesVentArr[i], _ventanasArr[i]);
            }
            DetectarDisparo(_personajePuerta, _puerta);
        }
        //* ui 
        _ui->Actualizar(_playerEmily->GetPuntos(), _playerEmily->GetVidas());

    }

    _sonido->Actualizar(_elapsed);

    //*condicion para perder el juego
    if (_playerEmily->GetVidas() == 0) {
        _juegoPerdido = true;
        _sonido->SetMusicaInGame(false);
        _sonido->SetMusicaGameOver();
        _gameOver->Actualizar(_elapsed);

    }

}

//***************************************************************************************************
//METODO PARA GESTIONAR LA ACTIVACION/DESACTIVACION DE LA LOGICA DE LOS PERSONAJES EN EL JUEGO
//setea la activacion o desactivacion de los personajes
// si la puerta esta cerrada los desactiva
//si la puerta esta abierta los activa(muestra un personaje al azar)
//****************************************************************************************************

void Juego::GestionPersonajes(Personajes* personaje, Aberturas* abertura) {

    if (!abertura-> GetEstaCerrada()) {
        personaje->SetActivarPersonaje();
    } else {
        personaje->SetDesactivarPersonaje();
    }
}

//**********************************************************************************************************
//METODO PARA DETECTAR ATAQUE DE PARTE DE LOS ENEMIGOS
//Si el personaje esta atacandonos setea el efecto para ver el garrazo y el sonido del mismo
//tambien resta vidas al jugador, 
//el personaje pega y se esconde( significa que se desactiva y cambia el estado de la abertura a cerrado)
//**********************************************************************************************************

void Juego::DetectarAtaque(Personajes* personaje, Aberturas* abertura) {

    if (personaje->EstaAtacando()) {
        _efectos->SetGarrazo();
        _sonido->SetSonidoGarrazo();
        _playerEmily->SetRestarVida();
        personaje->SetDesactivarPersonaje();
        abertura->CambiarEstado();
    }

}

//*************************************************************************************************************************
// METODO PARA DETECTAR SI CUANDO DISPARAMOS LE PEGAMOS A UN ENEMIGO O REHEN, SUMA PUNTO O RESTA VIDAS EN CONSECUENCIA
//este metodo se utiliza cuando disparamos(hacemos click con el mouse en algun lugar de la pantalla)
//se activa cuando la boleana _estaDisparando es true (se pone true significa que se esta disparando)
////////////////
//funcionamiento:
//verifica colision entre el rectangulo del sprite de los personajes y las coordenadas del disparo,
// y tambien verifica si la abertura esta ABIERTA mientras disparamos y el personaje esta vivo/activo
//si todo lo anterior se cumple pasa a verificar si es enemigo o rehen y suma puntos o resta vida segun corresponda
//sonido:tambien setea el sonido de herido para rehen o para enemigo
//Estado aberturas: por ultimo cambia el estado de las aberturas(si matamos a un personaje la ventana se cierra y se descativa el personaje)
//si no detecta colision pone la boleana _estaDiparando en false de nuevo
//***************************************************************************************************************************

void Juego::DetectarDisparo(Personajes* personaje, Aberturas* abertura) {

    //si hay colision entre las coordenadas de la mira y el rectangulo del personaje y la ventana esta abierta:
    if (personaje->GetSpriteBounds().contains(_playerEmily->GetCoordenadasMira())&& !abertura->GetEstaCerrada() && personaje->EstaActivo()) {
        //identifica si le pego a enemigo o rehen
        //si es enemigo:
        if (personaje->GetEsEnemigo()) {
            _playerEmily->SetSumarPuntos();
            _sonido->SetSonidoHerido(2);
        } else {
            //si es rehen:
            _playerEmily->SetRestarVida();
            _sonido->SetSonidoHerido(1);
        }
        personaje->SetDesactivarPersonaje();
        abertura->CambiarEstado();
    }
    _estaDisparando = false;

}

//*****************************************
//METODO PARA DIBUJAR
//dibuja todos los elementos del juego
//******************************************

void Juego::Dibujar(sf::RenderWindow *_window) {

    _window->clear(sf::Color(30, 6, 80, 255));

    if (!_juegoPerdido) {
        //dibujamos los elemementos del juego:

        //* dibuja personajes
        for (int i = 0; i < 5; i++) {
            _personajesVentArr[i]->Dibujar(_window);

        }
        _personajePuerta->Dibujar(_window);

        //* dibuja fondo
        _escenario->Dibujar(_window);

        //* dibuja ventanas
        for (int i = 0; i < 5; i++) {
            _ventanasArr[i]->Dibujar(_window);
        }
        _puerta->Dibujar(_window);

        //*dibuja efectos
        _efectos->Dibujar(_window);

        //* dibuja ui
        _ui->Dibujar(_window);

        //* dibuja la mira del player
        _playerEmily->Dibujar(_window);

    } else {
        _gameOver->Dibujar(_window);
    }


    _window->display();
}