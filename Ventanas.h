
/*
 * ES LA CLASE VENTANA
 * es hija de aberturas, por eso hereda sus propiedades y metodos
 * redefine su propia posicion 
 * redefine su propio dibujado decide segun el caso dibujar la ventana abierta o cerrada
 * asigna textura a sus sprites de venatana abierta y cerrada
 * 
 */

/* 
 * File:   Ventanas.h
 * Autora: Maria Emilia Corbetta
 *
 */

#ifndef VENTANAS_H
#define VENTANAS_H

//incluimos las liberias necesarias
#include <SFML/Graphics.hpp>
#include <iostream>

//incluimos el encabezado de la clase madre
#include "Aberturas.h"

class Ventanas : public Aberturas {
public:
    //constructor y destructor de la clase
    Ventanas();
    ~Ventanas();

    //metodos publicos
    void SetPosicionAberturas(int id);
    void Dibujar(sf::RenderWindow* _window);


private:
    //atributos privados
    sf::Texture* _texturaVentanaAbierta;
    sf::Texture* _texturaVentanaCerrada;

    sf::Sprite* _spriteVentanaAbierta;
    sf::Sprite* _spriteVentanaCerrada;

};

#endif /* VENTANAS_H */

