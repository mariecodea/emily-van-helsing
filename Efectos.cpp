
/* 
 * File:   Efectos.cpp
 * Autora: Maria Emilia Corbetta
 * 
 */

#include "Efectos.h"

//*********************************************
//CONTRUCTOR Y DESTRUCTOR DE LA CLASE
//*********************************************
//constructor

Efectos::Efectos() {
    //instanciamos los objetos texturas y sprites
    _texturaVignette = new sf::Texture;
    _spriteVignette = new sf::Sprite;
    _texturarejita = new sf::Texture;
    _spriteRejita = new sf::Sprite;

    for (int i = 0; i < 6; i++) {
        _garrazoFx[i] = new Garrazo;
    }

    //cargamos texturas y sprites
    _texturaVignette->loadFromFile("Sprites/vignette-940x540.png");
    _spriteVignette->setTexture(*_texturaVignette);
    _texturarejita->loadFromFile("Sprites/rejita.png");
    _spriteRejita->setTexture(*_texturarejita);
    _spriteRejita->setPosition(207.f, 201.f);

}

//destuctor

Efectos::~Efectos() {
    //libreramos la memoria
    delete(_texturaVignette);
    delete (_spriteVignette);
    delete(_texturarejita);
    delete (_spriteRejita);

    for (int i = 0; i < 6; i++) {
        delete(_garrazoFx[i]);
    }
}

//*********************************************
// METODO PARA ACTUALIZAR 
//*sirve para pasar el tiempo a la clase garrazo para
// que haga la transicion
//*********************************************

void Efectos::Actualizar(sf::Time _elapsed) {
    for (int i = 0; i < 6; i++) {
        _garrazoFx[i]->Actualizar(_elapsed);
    }
}

//****************************************************************************************
// METODO SETGARRAZO  (setter)
// *la clase juego hace el llamado de este metodo set cuando estan atacando los enemigos
//sirve para activar el efecto del garrazo 
//*****************************************************************************************

void Efectos::SetGarrazo() {
    for (int i = 0; i < 6; i++) {
        if (_garrazoFx[i]->GetEstaActivoGarrazo())continue;
        _garrazoFx[i]->SetActivarGarrazo();
        return;
    }
}

//*********************************************
// METODO PARA DIBUJAR LOS EFECTOS
//**********************************************

void Efectos::Dibujar(sf::RenderWindow* _window) {
    _window->draw(*_spriteVignette);
    _window->draw(*_spriteRejita);
    for (int i = 0; i < 6; i++) {
        _garrazoFx[i]->Dibujar(_window);
    }
}





