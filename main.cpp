//incluimos bibliotecas necesarias
#include <SFML/Graphics.hpp>
#include <ctime>
#include <cstdlib>

//incluimos headers de las clases
#include "Juego.h"

int main() {

    std::srand(std::time(0));
    
    Juego* _juego;
    _juego = new Juego;
    _juego->IniciarJuego();
    delete(_juego);
    

    return 0;
}