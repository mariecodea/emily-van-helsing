
/* 
 * File:   Sonido.cpp
 * Autora: Maria Emilia Corbetta
 */

#include "Sonido.h"
//******************************************
//CONTRUCTOR Y DESTRUCTOR DE LA CLASE
//******************************************
//constructor

Sonido::Sonido() {

    _bufferRehenHerido.loadFromFile("Sonidos/abueloouch2.ogg");
    _sonidoRehenHerido.setBuffer(_bufferRehenHerido);
    _sonidoRehenHerido.setVolume(50.f);

    _bufferEnemigoHerido.loadFromFile("Sonidos/enemiok.ogg");
    _sonidoEnemigoHerido.setBuffer(_bufferEnemigoHerido);
    _sonidoEnemigoHerido.setVolume(50.f);

    _bufferGarrazo.loadFromFile("Sonidos/garrazo.ogg");
    _sonidoGarrazo.setBuffer(_bufferGarrazo);
    _sonidoGarrazo.setVolume(100.f);

    _bufferGarrazo2.loadFromFile("Sonidos/garrazo1.ogg");
    _sonidoGarrazo2.setBuffer(_bufferGarrazo2);
    _sonidoGarrazo2.setVolume(100.f);

    _musicaGameOver.openFromFile("Sonidos/GameOver5.ogg");
    _musicaInGame.openFromFile("Sonidos/DanceHall.ogg");
    _musicaInGame.setVolume(25.f);
    _musicaInGame.setLoop(true);

    _suenaGameOver = false;
    _suenaMusicaInGame = false;
    _esSonidoEnemigo = false;
    _esSonidoRehen = false;
    _playUnaVezGamOv = false;
    _playUnaVezInGame = false;
    _playGarrazo = false;

}
//destructor

Sonido::~Sonido() {
}

//***************************************************************
//METODO PARA ACTUALIZAR
//***************************************************************

void Sonido::Actualizar(sf::Time _elapsed) {

    //logica para reproducir la musica in game
    if (!_suenaMusicaInGame)_musicaInGame.stop();
    if (_suenaMusicaInGame && !_playUnaVezInGame) {
        //std::cout << "MUSICA IN GAME" << std::endl;
        _musicaInGame.play();
        _playUnaVezInGame = true;
    }

    //logica para  reproducir la musica de game over
    if (_suenaGameOver && !_playUnaVezGamOv) {
        _tiempoAcumulado += _elapsed;
        if (_tiempoAcumulado.asSeconds() > 1.5) {
            _musicaGameOver.play();
            //std::cout << "JAJAJAJ" << std::endl;
            _suenaGameOver = false;
            _playUnaVezGamOv = true;
        }
    }

    //logica para que suene el garrazo
    if (_playGarrazo) {
        if (aleatorio == 0)_sonidoGarrazo.play();
        if (aleatorio == 1) _sonidoGarrazo2.play();
        _playGarrazo = false;
    }

    //logica para que suene el sonido del rehen herido
    //si flag del rehen es verdadero
    if (_esSonidoRehen) {
        //std::cout << "SONIDO OOOOOOOUUUUUCHHHHHHHHH" << std::endl;
        _sonidoRehenHerido.play();
        _esSonidoRehen = false;
    }
    //logica para que suene el sonido del enemigo herido
    //si flag del enemigo true 
    if (_esSonidoEnemigo) {
        // std::cout << "SONIDO GRAAAYYYYYYYYYY" << std::endl;
        _sonidoEnemigoHerido.play();
        _esSonidoEnemigo = false;

    }
}
//***************************************************************
//METODO SET MUSICA IN GAME  (setter)
//se llama desde la clase Juego cuando empieza a correr el programa
//sirve para  indicar que empiece a correr o no la musica in game
//parametro es una boleana que se guarda en una variable  booleana 
//local que se usa en actualizar para reproducir/parar la musica
//***************************************************************

void Sonido::SetMusicaInGame(bool ponePlay) {
    _suenaMusicaInGame = ponePlay;
}
//***************************************************************
//METODO SET GAME OVER (setter)
//se llama desde la clase Juego cuando las vidas estan en cero
//sirve para  indicar que empiece a correr o no la musica Game Over
//***************************************************************

void Sonido::SetMusicaGameOver() {
    _suenaGameOver = true;
}
//***************************************************************
//METODO SET SONIDO GARRAZO (setter)
//se llama desde la clase Juego cuando se detecta al enemigo atacando
//sirve para  indicar que empiece a correr o no EL SONIDO del garrazo
//al hacer el llamado de este metodo se setea a true  _playGarrazo.
//Hay dos sonidos del garrazo: para ver cual suena elige un numero
//al azar entre 0 y 1 y se guarda en la variable aleatorio
//***************************************************************

void Sonido::SetSonidoGarrazo() {
    _playGarrazo = true;
    aleatorio = rand() % 2;
}
//***************************************************************
//METODO SET SONIDO HERIDO (setter)
//este metodo se llama en la clase juego cuando hay colision entre
//un personaje y la bala y se le pasa como argumento un int= 1 o 2
//si _quienEs=1 es un rehen
//si _quienEs=2 es un enemigo
//dependiendo el caso se activa la boleana en true para reproducir
//el sonido de personaje herido
//***************************************************************

void Sonido::SetSonidoHerido(int _quienEs) {
    //caso 1
    //si es abuelo setea flag del rehen a true
    if (_quienEs == 1) {
        _esSonidoRehen = true;
    }
    //caso 2
    //si es enemigo setea flag del enemigo a true
    if (_quienEs == 2) {
        _esSonidoEnemigo = true;
    }

}



