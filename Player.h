
/*
 * CLASE PLAYER
 * esta clase posiciona la mira del jugador
 * devuleve las coordenads actuales de la mira en un vector
 * suma puntos y devuelve el valor de puntos
 * resta vida y devuleve el valor de vida
 * dibuja y actualiza
 * 
 */

/* 
 * File:   Player.h
 * Autora: Maria Emilia Corbetta
 */

#ifndef PLAYER_H
#define PLAYER_H

//incluimos las bibliotecas necesarias
#include <SFML/Graphics.hpp>
#include <iostream>


class Player {
public:
    //constructor y destructor
    Player();
    ~Player();
    
    //metodos publicos
    void SetCoordenadasMouse(float x, float y);
    sf::Vector2f GetCoordenadasMira();
    void SetRestarVida();
    int GetVidas();
    void SetSumarPuntos();
    int GetPuntos(); void Actualizar();
    void Dibujar(sf::RenderWindow* _window);
     
private:
    //atributos privados
    sf::Texture* _texturaMira;
    sf::Sprite* _spriteMira;
    
    sf::Vector2f coords;
    int puntos;
    int vidas;
    
    //metodos privado
     void PosicionarMira();
    
};

#endif /* PLAYER_H */

