

/* 
 * File:   GameOver.h
 * Autora: Maria Emilia Corbetta
 *
 */

#ifndef GAMEOVER_H
#define GAMEOVER_H

//incluimos las liberias necesarias
#include <SFML/Graphics.hpp>
#include <iostream>

class GameOver {
public:
    //constructor/destructor
    GameOver();
    ~GameOver();
    //metodos publicos
    void Actualizar(sf::Time _elapsed);
    void Dibujar(sf::RenderWindow* _window);
    
private:
    //atributos privados
    sf::Texture *_texturaGameOver;
    sf::Sprite *_spriteGameOver;
    sf::Time tiempoAcumulado;
    bool juegoPerdido;
   
};

#endif /* GAMEOVER_H */

