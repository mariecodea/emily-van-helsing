
/* 
 * File:   Personajes.cpp
 * Author: Maria Emilia Corbetta
 */

#include "Personajes.h"

//*******************************************
//CONTRUCTOR Y DESTRUCTOR DE LA CLASE
//*********************************************
//////constructor

Personajes::Personajes() {
    //instanciamos los objetos sprite y textura
    _texturaDracula = new sf::Texture;
    _spriteDracula = new sf::Sprite;
    _texturaLobo = new sf::Texture;
    _spriteLobo = new sf::Sprite;
    _texturaAbuela = new sf::Texture;
    _spriteAbuela = new sf::Sprite;
    _texturaAbuelo = new sf::Texture;
    _spriteAbuelo = new sf::Sprite;

    //Cargamos las texturas y  ponemos texturas sprites
    _texturaDracula->loadFromFile("Sprites/vampiro-solo-parado-84x102.png");
    _spriteDracula->setTexture(*_texturaDracula);
    _texturaLobo->loadFromFile("Sprites/werewolf-solo-parado-72x112.png");
    _spriteLobo->setTexture(*_texturaLobo);
    _texturaAbuela->loadFromFile("Sprites/abuela-49x108.png");
    _spriteAbuela->setTexture(*_texturaAbuela);
    _texturaAbuelo->loadFromFile("Sprites/abuelo-49x108.png");
    _spriteAbuelo->setTexture(*_texturaAbuelo);

    personajeActivo = -100; // asigno numero negativo cualquiera para indicar que no hay ningun personaje activado
    _activarPersonaje = false;
    _estaAtacando = false;
    _atacoUnaVez = false;
    _tiempoAcumulado = sf::seconds(0.f);
}

//destructor

Personajes::~Personajes() {
    delete(_spriteDracula);
    delete(_texturaDracula);
    delete(_spriteLobo);
    delete(_texturaLobo);
    delete(_spriteAbuela);
    delete(_texturaAbuela);
    delete(_spriteAbuelo);
    delete(_texturaAbuelo);
}

//*************************************************************************
//METODO PARA POSICIONAR A LOS PERSONAJES  EN LAS ABERTURAS (setter)
// * Chequea el valor de la variable posicion
// * Depende del valor setea la posicion del sprite en la posicion adecuada 
// * casos del 0 al 5 ( 0,1,2,3,4 ventanas y 5 es la puerta)
// * esquema de posicion de la puerta y ventana en el siguiente dibujo:
//   //----------\\
//    |0   1   2 |
//    |5   3   4 |
//    ************
//***********************************************************************

void Personajes::SetPosicionPersonajes(int posicion) {

    switch (posicion) {

        case 0: //posiciona en ventana 0
            _spriteAbuela->setPosition(251.f, 117.f);
            _spriteAbuelo->setPosition(251.f, 117.f);
            _spriteLobo->setPosition(242.f, 113.f);
            _spriteDracula->setPosition(233.f, 123.f);
            break;

        case 1://posiciona en ventana 1
            _spriteAbuela->setPosition(413.f, 117.f);
            _spriteAbuelo->setPosition(413.f, 117.f);
            _spriteLobo->setPosition(401.f, 113.f);
            _spriteDracula->setPosition(395.f, 123.f);
            break;

        case 2://posiciona en ventana 2
            _spriteAbuela->setPosition(551.f, 117.f);
            _spriteAbuelo->setPosition(551.f, 117.f);
            _spriteLobo->setPosition(539.f, 113.f);
            _spriteDracula->setPosition(533.f, 123.f);
            break;


        case 3: //posiciona en ventana 3
            _spriteAbuela->setPosition(407.f, 345.f);
            _spriteAbuelo->setPosition(407.f, 345.f);
            _spriteLobo->setPosition(396.f, 341.f);
            _spriteDracula->setPosition(390.f, 351.f);
            break;

        case 4://posiciona en ventana 4
            _spriteAbuela->setPosition(546.f, 345.f);
            _spriteAbuelo->setPosition(546.f, 345.f);
            _spriteLobo->setPosition(534.f, 341.f);
            _spriteDracula->setPosition(529.f, 351.f);
            break;

        case 5: //es la puerta
            _spriteAbuela->setPosition(258.f, 345.f);
            _spriteAbuelo->setPosition(258.f, 345.f);
            _spriteLobo->setPosition(246.f, 341.f);
            _spriteDracula->setPosition(240.f, 351.f);
            break;
    }
}

//*************************************************************************************************
//METODO SETACTIVARPERSONAJE (setter)
// se llama en la clase Juego
//elige un numero random  para poder representar a un personaje al azar
// inicia el contador de tiempo a cero para poder determinar cuando atacar si es un enemigo
//***************************************************************************************************

void Personajes::SetActivarPersonaje() {
    if (personajeActivo >= 0) return;
    personajeActivo = std::rand() % 4; //elije un numero random entre 0 y 3
    _tiempoAcumulado = sf::seconds(0.f); //ponemos contador de tiempo a cero
}

//************************************************************************************************************
//METODO SETDESACTIVARPERSONAJE (setter)
//sirve para desactivar el personaje activo (pone en false las boleanas
// asigna nuevamente valor por defecto -100 para personajeActivo(significa que no hay ningun personaje asignado
//se usa llama en juego para cuando un personaje muere o ya termino de atacar
//***************************************************************************************************************

void Personajes::SetDesactivarPersonaje() {
    if (personajeActivo < 0)return; // si es -100, no hay ningun personaje activado, se vuelve
    personajeActivo = -100;
    _estaAtacando = false;
    _atacoUnaVez = false;
}

//**************************************************************************************
//METODO ESTAATACANDO 
//sirve para saber si el enemigo esta atacando actualmente o no
//sirve para saber su atacó que no ataque de nuevo( solo debe atacar una vez)
//se usa en la clase juego en el metodo para detectar ataques del enemigo
//devuelve verdadero o falso
//****************************************************************************************

bool Personajes::EstaAtacando() {

    if (_estaAtacando && !_atacoUnaVez) {
        _atacoUnaVez = true;
        return true;
    } else {
        return false;
    }
}

//*********************************************************************************************************
//METODO GETESENEMIGO (getter)
//devuleve si el pesonaje activo actualmente es un enemigo(true) o no(false)
//sirve para determinar si el personaje activo ataca (si es enemigo)o no (si es rehen)
//se usa en el metodo actualizar de esta clase
//tambien se usa en juego cuando detecta que colision de disparo a un personaje, para saber a quien disparó
//**********************************************************************************************************

bool Personajes::GetEsEnemigo() {
    if (personajeActivo == 0 || personajeActivo == 1)return true;
    else return false;
}

//********************************************************************************
//METODO ESTA ACTIVO
//devuelve verdadero o falso si hay algun personaje vivo/activo
//para eso verifica  el valor de la variable personajeActivo
//valor: valor de 0 a 4=true, valor menor a cero no hay personaje activo= false
//********************************************************************************

bool Personajes::EstaActivo() {
    if (personajeActivo < 0) return false;
    return true;
}

//*******************************************************************************
//METODO GETSPRITEBOUNDS (getter)
//devuelve el tamaño que ocupa el rectangulo del sprite del personaje
///////
//Observacion
//tomamos la medida del rectangulo del sprite de la abuela como medida estandar, sirve para todos
//el sprite del abuelo mide igual al de la abuela
//el sprite de los enemigos es mas grande pero esta bien medirlo con el de la abuela 
//por que es mas chico , por lo tanto requiere mas precision y sirve para el juego.
//**********************************************************************************

sf::FloatRect Personajes::GetSpriteBounds() {

    return _spriteAbuela->getGlobalBounds();
}

//******************************************************************
//METODO PARA ACTUALIZAR 
//verifica si hay un personaje activo y si este es un enemigo
//Mide el tiempo y activa el ataque de un enemigo
//*****************************************************************

void Personajes::Actualizar(sf::Time _elapsed) {

    if (personajeActivo >= 0 && GetEsEnemigo()) {
        _tiempoAcumulado += _elapsed;

        if (_tiempoAcumulado.asSeconds() > 2.0f) {
            _estaAtacando = true;

        }
    }
}
//***************************************************************************************
//METODO PARA DIBUJAR A LOS PERSONAJES 
//dibuja algun personaje segun el valor aleatorio de la variable personajeActivo 
//****************************************************************************************

void Personajes::Dibujar(sf::RenderWindow* _window) {

    switch (personajeActivo) {
        case 0: //es dracula
            _window->draw(*_spriteDracula);
            break;
        case 1: // es el lobo
            _window->draw(*_spriteLobo);
            break;
        case 2: //es la abuela
            _window->draw(*_spriteAbuela);
            break;
        case 3: //es el abuelo
            _window->draw(*_spriteAbuelo);
            break;
    }
}



