
/* 
 * File:   Puerta.cpp
 * Autora: Maria Emilia Corbetta
 * 
 */

#include "Puerta.h"

//******************************************************************************
//CONSTRUCTOR  Y DESTRUCTOR DE LA CLASE
//*****************************************************************************
//constructor

Puerta::Puerta() {
    //instanciamos los objetos sprite y textura
    _texturaPuertaAbierta = new sf::Texture;
    _spritePuertaAbierta = new sf::Sprite;
    _texturaPuertaCerrada = new sf::Texture;
    _spritePuertaCerrada = new sf::Sprite;

    //Cargamos las texturas y seteamos esas texturas a los sprites
    _texturaPuertaAbierta->loadFromFile("Sprites/puerta-abierta-168x188.png");
    _spritePuertaAbierta->setTexture(*_texturaPuertaAbierta);
    _texturaPuertaCerrada->loadFromFile("Sprites/puerta-84x168.png");
    _spritePuertaCerrada->setTexture(*_texturaPuertaCerrada);
}
//destructor

Puerta::~Puerta() {
    delete(_spritePuertaAbierta);
    delete(_texturaPuertaAbierta);
    delete(_spritePuertaCerrada);
    delete(_texturaPuertaCerrada);
}

//******************************************************************************
//METODO PARA POSICIONAR LA PUERTA (setter)
// recibe un valor de id para identificar si la abertura esta abierta o cerrada 
// y setea la posicion correcta de acuerdo a ese id
//aclaracion la imagen de la puerta abierta y cerrada no miden lo mismo
//por eso se hace esta distincion para poder posicionarlas bien
//*****************************************************************************

void Puerta::SetPosicionAberturas(int idAbertura) {
    if (idAbertura == 0)_spritePuertaCerrada->setPosition(239.f, 286.f);
    else _spritePuertaAbierta->setPosition(200.f, 265.f);
}

//******************************************************************************
//METODO PARA DIBUJAR
//*****************************************************************************

void Puerta::Dibujar(sf::RenderWindow* _window) {
    if (_estaCerrada) {
        SetPosicionAberturas(0);
        _window->draw(*_spritePuertaCerrada);
    } else {
        SetPosicionAberturas(1);
        _window->draw(*_spritePuertaAbierta);
    }
}


