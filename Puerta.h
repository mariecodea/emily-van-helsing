/*
 * ES LA CLASE PUERTA
 * es hija de aberturas, por eso hereda sus propiedades y metodos
 * redefine su propia posicion 
 * redefine su propio dibujado decide segun el caso dibujar la puerta abierta o cerrada
 * asigna textura a sus sprites de puerta abierta y cerrada
 * 
 */

/* 
 * File:   Puerta.h
 * Autora: Maria Emilia Corbetta
 *
 */

#ifndef PUERTA_H
#define PUERTA_H

//incluimos las liberias necesarias
#include <SFML/Graphics.hpp>
#include <iostream>

//incluimos el encabezado de la clase madre
#include "Aberturas.h"


class Puerta:public Aberturas {
public:
    //constructor y destructor
    Puerta();
    ~Puerta();
    
    //metodos publicos
    void Dibujar(sf::RenderWindow* _window);
    void SetPosicionAberturas(int idAbertura);
                                              
private:
    sf::Texture* _texturaPuertaAbierta;
    sf::Sprite* _spritePuertaAbierta;

    sf::Texture* _texturaPuertaCerrada;
    sf::Sprite* _spritePuertaCerrada;
};

#endif /* PUERTA_H */

