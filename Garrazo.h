
/*
 * ESTA CLASE ES GARRAZO
 * la usa solamente la clase efecto
 * el garrazo es un efecto visual que aparece cuando un enemigo pega al jugador/a
 * esta clase sirve para manejar el efecto del garrazo:
 * carga las texturas para los sprite garrazo
 * aleatorizamos los sprite garrazo( detalle visual para que no salga siempre el mismo)
 * hace un efecto  de transicion fade out que dura un determinado tiempo que vamos a medir
 * 
 */

/* 
 * File:   Garrazo.h
 * Autora: Maria Emilia Corbetta
 */

#ifndef GARRAZO_H
#define GARRAZO_H

//incluimos las liberias necesarias
#include <SFML/Graphics.hpp>
#include <iostream>

class Garrazo {
public:
    //contructor/destructor
    Garrazo();
    ~Garrazo();
    
    //metodos publicos
    void Actualizar(sf::Time _elapsed);
    void Dibujar(sf::RenderWindow* _window);
    void SetActivarGarrazo();
    bool GetEstaActivoGarrazo();

private:
    //atributos privados
    sf::Texture* _texturaGarrazzo;
    sf::Sprite* _spriteGarrazo;

    sf::Texture* _texturaGarrazzo2;
    sf::Sprite* _spriteGarrazo2;

    bool _garrazoActivo;
    sf::Time tiempoAcumuladoGarrazo;
    int alpha;
    int aleatorio;

};

#endif /* GARRAZO_H */

