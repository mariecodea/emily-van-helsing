
/* 
 * File:   Aberturas.cpp
 * Autora: Maria Emilia Corbetta
 */

#include "Aberturas.h"


//*********************************************
//CONTRUCTOR Y DESTRUCTOR DE LA CLASE
//*********************************************
Aberturas::Aberturas() {
    _estaCerrada = true;
    tiempoDeIntervalo = (rand() % 3) + 3;
}

Aberturas::~Aberturas() {
}

//*****************************************************
//METODO GETESTACERRADA
//devuleve el estado abierto/cerrado de una abertura
//******************************************************
bool Aberturas::GetEstaCerrada(){
    if (_estaCerrada) {        
        return true; //esta cerrada
    } else {
        return false; //esta abierta
    }
}

//*********************************************************
//METODO PARA CAMBIAR ESTADO
//sirve para cambiar el estado de abierto/cerrado
//*********************************************************
void Aberturas::CambiarEstado() {
    if (_estaCerrada)_estaCerrada = false;
    else _estaCerrada = true;
    _tiempoAcumulado = sf::seconds(0.f); //reinicia a cero el tiempo acumulado
}

//******************************************************************************
//METODO PARA ACTUALIZAR
// se usa para gestionar la logica cuando se abren y cierran las aberturas
// medimos el tiempo transcurrido y lo compara con un tiempo limite de intervalo
//que definimos aleatoriamente para saber cuando cambiar de estado las aberturas
//******************************************************************************
void Aberturas::Actualizar(sf::Time _elapsed) {
    
    //acumulamos el tiempo transcurrido
    _tiempoAcumulado += _elapsed;
    
    //verificamos si el tiempo acumulado es mayor al tiempo de intervalo random
    if (_tiempoAcumulado.asSeconds() > tiempoDeIntervalo) {
        _tiempoAcumulado = sf::seconds(0.f);  //reinicia a cero el tiempo acumulado
         tiempoDeIntervalo = (rand() % 3) + 3;
        CambiarEstado();
    }

}

