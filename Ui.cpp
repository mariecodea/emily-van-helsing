
/* 
 * File:   Ui.cpp
 * Autora: Maria Emilia Corbetta
 */

#include "Ui.h"
//***************************************************************
//CONSTRUCTOR Y DESTRUCTOR DE LA CLASE
//***************************************************************
//constructor

Ui::Ui() {
    //instanciamos los objetos sprite y textura
    _texturaRetrato = new sf::Texture;
    _spriteRetrato = new sf::Sprite;
    _texturaCorazon = new sf::Texture;
    _spriteCorazon = new sf::Sprite;

    //setamos textura y la cargamos a los sprites
    _texturaRetrato->loadFromFile("Sprites/emily-portrait.png");
    _spriteRetrato->setTexture(*_texturaRetrato);
    _spriteRetrato->setPosition(8.f, 6.f);
    _texturaCorazon->loadFromFile("Sprites/corazon-21x20.png");
    _spriteCorazon->setTexture(*_texturaCorazon);

    //seteamos la fuente y el texto
    //carga la fuente
    _miFuente.loadFromFile("Font/MayfairNbpBold-gAA4.ttf");

    //setea la fuente para el texto Titulo de puntaje
    tituloPuntaje.setFont(_miFuente);
    tituloPuntaje.setCharacterSize(38);
    tituloPuntaje.setFillColor(sf::Color::White);
    tituloPuntaje.setPosition(380.f, 4.f);
    tituloPuntaje.setString("Puntaje: ");

    //setea la fuente para los puntos
    strPuntos.setFont(_miFuente);
    strPuntos.setCharacterSize(38);
    strPuntos.setFillColor(sf::Color::White);
    strPuntos.setPosition(570.f, 6.f);
    strPuntos.setString("0");

    //carga las variable punto y vida con valor inicial
    puntos = 0;
    vidas = 3;

}
//destructor

Ui::~Ui() {
    delete(_spriteRetrato);
    delete(_texturaRetrato);
    delete(_spriteCorazon);
    delete(_texturaCorazon);
}
//***************************************************************
//METODO PARA CONVERTIR UN INT A STRING
//*sirve para convertir el int que guardamos en puntos a un string
//*se almacena en la variable de texto strPuntos
//***************************************************************

void Ui::ConvertirIntAString() {

    std::stringstream convertidor;

    convertidor << std::setw(3) << std::setfill('0') << puntos;
    strPuntos.setString(convertidor.str());

}
//***************************************************************
//METODO PARA ACTUALIZAR
//Actualiza los valores de puntos, vidas
// hace llamadas al metodo que convierte de int a string
//***************************************************************

void Ui::Actualizar(int _puntos, int _vidas) {

    puntos = _puntos;
    vidas = _vidas;
    ConvertirIntAString();

}
//***************************************************************
//METODO PARA DIBUJAR 
//dibuja sprites y los textos para ver el puntaje
//***************************************************************

void Ui::Dibujar(sf::RenderWindow* _window) {
    //dibuja corazones y setea la posicion de acuerdo a cuantas vidsa tiene
    pos = 19;
    for (int i = 0; i < vidas; i++) {
        _window->draw(*_spriteCorazon);
        _spriteCorazon->setPosition(pos, 126.f);
        pos += 30;
    }

    _window->draw(*_spriteRetrato);
    _window->draw(tituloPuntaje);
    _window->draw(strPuntos);

}