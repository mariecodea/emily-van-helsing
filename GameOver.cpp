
/* 
 * File:   GameOver.cpp
 * Autora: Maria Emilia Corbetta
 */

#include "GameOver.h"

//*****************************************************
//CONSTRUCTOR Y DESTRUCTOR DE LA CLASE
//*****************************************************
//constructor
GameOver::GameOver() {
    //instanciamos los objetos sprite y textura
    _texturaGameOver = new sf::Texture;
    _spriteGameOver = new sf::Sprite;

    //cargamos la textura y el sprite con textura
    _texturaGameOver->loadFromFile("Sprites/game-over-lobo-940x540.png");
    _spriteGameOver->setTexture(*_texturaGameOver);
    juegoPerdido = false;

}

//destructor

GameOver::~GameOver() {
    delete (_spriteGameOver);
    delete (_texturaGameOver);
}

//*****************************************************
//METODO QUE SE ACTUALIZA EN LA CLASE JUEGO
//*Cuando la vida es igual a cero se hace el llamado a este
//metdo y empieza un contador que cuando cuenta 1.5 seg
//activa el flag JuegoPerdido como verdadero
//*****************************************************

void GameOver::Actualizar(sf::Time _elapsed) {
    tiempoAcumulado += _elapsed;
    //activa despues de 1.5 seg el flag para mostrar pantalla de gameOver
    if (tiempoAcumulado.asSeconds() > 1.5f && !juegoPerdido) {
        juegoPerdido = true;
    }
}

//**********************************************************************
//METODO PARA DIBUJAR EL FONDO DE GAME OVER cuando se pierde el juego
// *dibuja el fondo de game over cuando el flag juegoPerdido es true
//**********************************************************************

void GameOver::Dibujar(sf::RenderWindow* _window) {
    _window->clear();
    if (juegoPerdido)_window->draw(*_spriteGameOver);
}


