
/*ESTA ES LA CLASE UI
 * muestra sprite del retrato de Emily y los corazones que indican la vida del player
 * tambien se encarga de actualizar, mostrar los puntos y vidas 
 * 
 */

/* 
 * File:   Ui.h
 * Autora: Maria Emilia Corbetta
 *
 */

#ifndef UI_H
#define UI_H

//incluimos las liberias basicas necesarias
#include <SFML/Graphics.hpp>
#include <iostream>

//bibliotecas para manipular string se necesita para convertir int a string
#include <sstream>
#include<iomanip>

class Ui {
public:
    //constructor y destructor de la clase
    Ui();
    ~Ui();

    //metodos publicos
    void Actualizar(int puntos, int vidas);
    void Dibujar(sf::RenderWindow* _window);

private:

    //Atributos privados
    //sprites y texturas
    sf::Texture* _texturaRetrato;
    sf::Sprite* _spriteRetrato;

    sf::Texture* _texturaCorazon;
    sf::Sprite* _spriteCorazon;

    //fuente y textos para mostrar puntaje en la ui
    sf::Font _miFuente;
    sf::Text tituloPuntaje;
    sf::Text strPuntos;

    //variables
    int puntos;
    int vidas;
    float pos;

    //metodos privados
    void ConvertirIntAString();

};

#endif /* UI_H */

