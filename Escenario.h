/*
 * ESTA CLASE  SIRVE PARA EL FONDO DEL JUEGO
 * como fondo entendemos el cielo,los arboles, la casa
 * la imagen de fondo tiene las aberturas en alpha para poder meter
 * atras a los personajes
 */

/* 
 * File:   Escenario.h
 * Autora: Maria Emilia Corbetta
 *
 */

#ifndef ESCENARIO_H
#define ESCENARIO_H

//incluimos las librerias necesarias
#include <SFML/Graphics.hpp>

class Escenario {
public:
    //constructor/destructor 
    Escenario();
    ~Escenario();

    //metodos publicos
    void Dibujar(sf::RenderWindow* _window);

private:
    //atributos privados
    sf::Texture* _texturaFondoCasa;
    sf::Sprite* _spriteFondoCasa;

};

#endif /* ESCENARIO_H */

