
/*CLASE PARA EFECTOS DEL JUEGO:
 *  efecto del garrazo (transicion fade out)
 *  efecto viñeteado
 *  tambien esta la reja del segundo piso de la casa que sirve como fix grafico
 */


/* 
 * File:   Efectos.h
 * Autora: Maria Emilia Corbetta
 *
 */

#ifndef EFECTOS_H
#define EFECTOS_H

//incluimos las librerias necesarias
#include <SFML/Graphics.hpp>
#include <iostream>

//incluimos el encabezado de la clase Garrazo
#include "Garrazo.h"

class Efectos {
public:
    //constructor y destructor
    Efectos();
    ~Efectos();

    //metodos publicos
    void Actualizar(sf::Time _elapsed);
    void Dibujar(sf::RenderWindow* _window);
    void SetGarrazo();

private:
    //atributos pirvados
    sf::Texture* _texturaVignette;
    sf::Sprite* _spriteVignette;
    sf::Texture* _texturarejita;
    sf::Sprite* _spriteRejita;

    //objeto que es un array del tipo Garrazo
    //tenemos 6 objetos garrazo(uno por abertura) 
    //cada uno independiente del otro
    Garrazo* _garrazoFx[6];

};

#endif /* EFECTOS_H */

