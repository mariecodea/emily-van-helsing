/*
 * ESTA ES LA CLASE SONIDO
 * esta clase tiene los sonidos y musica del juego:
 * ***sonidos de personajes y rehenes heridos
 * ***sonido del garrazo
 * ***musica de game over
 * ***musica in game
 * 
 * mide el tiempo para iniciar la musica de game over con unos segundos de retraso para dar un pequeño silencio entre musica in game y game over
 * tiene principalmente metodos setters para indicar cuando inicializar la musica y sonidos
 * 
 */


/* 
 * File:   Sonido.h
 * Autora: Maria Emilia Corbetta
 */

#ifndef SONIDO_H
#define SONIDO_H

//incluimos la libreria de audio
#include <SFML/Audio.hpp>
#include <iostream>

class Sonido {
public:
    //constructor destructor de clase
    Sonido();
    ~Sonido();

    //metodos publicos
    void SetMusicaGameOver();
    void SetSonidoHerido(int _quienEs);
    void SetSonidoGarrazo();
    void SetMusicaInGame(bool ponePlay);
    void Actualizar(sf::Time _elapsed);
    
private:
    //sonidos
    sf::SoundBuffer _bufferRehenHerido;
    sf::Sound _sonidoRehenHerido;

    sf::SoundBuffer _bufferEnemigoHerido;
    sf::Sound _sonidoEnemigoHerido;

    sf::SoundBuffer _bufferGarrazo;
    sf::Sound _sonidoGarrazo;

    sf::SoundBuffer _bufferGarrazo2;
    sf::Sound _sonidoGarrazo2;

    //musica
    sf::Music _musicaGameOver;
    bool _suenaGameOver;

    sf::Music _musicaInGame;
    bool _suenaMusicaInGame;

    bool _esSonidoEnemigo;
    bool _esSonidoRehen;
    bool _playUnaVezGamOv;
    bool _playUnaVezInGame;
    bool _playGarrazo;
    int aleatorio;
    sf::Time _tiempoAcumulado;

};

#endif /* SONIDO_H */

