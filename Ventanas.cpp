
/* 
 * File:   Ventanas.cpp
 * Autora: Maria Emilia Corbetta
 * 
 */

#include "Ventanas.h"

//******************************************************************************
//CONSTRUCTOR  Y DESTRUCTOR DE LA CLASE
//*****************************************************************************
//constructor

Ventanas::Ventanas() {
    //instanciamos los objetos sprite y textura
    _texturaVentanaAbierta = new sf::Texture;
    _spriteVentanaAbierta = new sf::Sprite;
    _texturaVentanaCerrada = new sf::Texture;
    _spriteVentanaCerrada = new sf::Sprite;

    //Cargamos las texturas y seteamos esas texturas a los sprites
    _texturaVentanaAbierta->loadFromFile("Sprites/ventana-abierta-138x122.png");
    _spriteVentanaAbierta->setTexture(*_texturaVentanaAbierta);
    _texturaVentanaCerrada->loadFromFile("Sprites/ventana-cerrada-90x126.png");
    _spriteVentanaCerrada->setTexture(*_texturaVentanaCerrada);

}
//destructor

Ventanas::~Ventanas() {
    delete(_spriteVentanaAbierta);
    delete(_texturaVentanaAbierta);
    delete(_spriteVentanaCerrada);
    delete(_texturaVentanaCerrada);
}
//******************************************************************************
//METODO PARA POSICIONAR LA VENTANA(setter)
// recibe un valor de id para identificar si la abertura esta abierta o cerrada 
// y setea la posicion correcta de acuerdo a ese id
//aclaracion la imagen de la ventana abierta y cerrada no miden lo mismo
//por eso se hace esta distincion para poder posicionarlas bien
//*****************************************************************************

void Ventanas::SetPosicionAberturas(int idAbertura) {

    switch (idAbertura) {
        case 0: //ventana0
            _spriteVentanaAbierta->setPosition(210.f, 94.f);
            _spriteVentanaCerrada->setPosition(232.f, 94.f);
            break;

        case 1://ventana1
            _spriteVentanaAbierta->setPosition(370.f, 94.f);
            _spriteVentanaCerrada->setPosition(392.f, 94.f);
            break;

        case 2://ventana2
            _spriteVentanaAbierta->setPosition(508.f, 94.f);
            _spriteVentanaCerrada->setPosition(530.f, 94.f);
            break;

        case 3: //ventana3
            _spriteVentanaAbierta->setPosition(365.f, 311.f);
            _spriteVentanaCerrada->setPosition(387.f, 311.f);
            break;

        case 4: //ventana4
            _spriteVentanaAbierta->setPosition(503.f, 311.f);
            _spriteVentanaCerrada->setPosition(525.f, 311.f);
            break;

    }


}

//******************************************************************************
//METODO PARA DIBUJAR
//*****************************************************************************

void Ventanas::Dibujar(sf::RenderWindow* _window) {
    if (_estaCerrada) {
        _window->draw(*_spriteVentanaCerrada);
    } else {
        _window->draw(*_spriteVentanaAbierta);
    }
}

