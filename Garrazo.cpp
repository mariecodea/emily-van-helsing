
/* 
 * File:   Garrazo.cpp
 * Autora: Maria Emilia Corbetta
 */

#include "Garrazo.h"

//*******************************************
//CONTRUCTOR Y DESTRUCTOR DE LA CLASE
//*******************************************
//constructor

Garrazo::Garrazo() {
    //instanciamos los objetos sprite y textura
    _texturaGarrazzo = new sf::Texture;
    _spriteGarrazo = new sf::Sprite;
    _texturaGarrazzo2 = new sf::Texture;
    _spriteGarrazo2 = new sf::Sprite;

    //Cargamos las texturas y  ponemos texturas sprites
    _texturaGarrazzo->loadFromFile("Sprites/garrazo1.png");
    _spriteGarrazo->setTexture(*_texturaGarrazzo);
    _texturaGarrazzo2->loadFromFile("Sprites/garrazo2.png");
    _spriteGarrazo2->setTexture(*_texturaGarrazzo2);

    _garrazoActivo = false;
    tiempoAcumuladoGarrazo = sf::seconds(0.f);
    alpha = 255;
}

//destructor

Garrazo::~Garrazo() {
    delete (_spriteGarrazo);
    delete (_texturaGarrazzo);
    delete (_spriteGarrazo2);
    delete (_texturaGarrazzo2);
}

//*************************************************************************************************************
//METODO SETACTIVARGARRAZO (setter)
//se hace el llamado a este metodo desde la clase Efectos cuando un enemigo pega y necesitamos ver el efecto
//cuando se la llama : setea un flag en verdadero para actualizar y mostrar el garrazo,
//otorga una posicion aletaoria en X e Y a los garrazos para que no salga siempre en el mismo lugar
//elige un numero random entre 0 y 1 , este nos sirve para saber que sprite mostrar
//**************************************************************************************************************

void Garrazo::SetActivarGarrazo() {
    _garrazoActivo = true; //este es el flag  que activa al garrazo
    _spriteGarrazo->setPosition(0 - (rand() % -10) + 24, 0 - (rand() % -90) + 22);
    _spriteGarrazo2->setPosition(0 - (rand() % -10) + 24, 0 - (rand() % -90) + 22);
    aleatorio = rand() % 2;
    alpha = 255;
}

//**************************************************************************************************************************
//METODO GETESTAACTIVADO (getter)
//devuleve el valor de la boleana_garrazoActivo para saber si esta activado o no el garrazo
//este metodo se llama en la clase Efectos para saber si el garrazo esta activo o no
// RECORDAR:el objeto garrazo de la clase efectos es un array que tiene el estado de activado o no en cada posicion del mismo
// si no esta activado se activa esa posicion
//****************************************************************************************************************************

bool Garrazo::GetEstaActivoGarrazo() {
    return _garrazoActivo;
}

//**************************************************************************************************
//METODO PARA ACTUALIZAR
//si es _garrazoActivo es verdadero contamos el tiempo, no sirve para manejar la duracion del fade out
//restamos el valor alpha para hacer la transicion fade out (jugamos con la transparencia)
//reiniciamos la variable tiempoAcumulado  a cero
//**************************************************************************************************

void Garrazo::Actualizar(sf::Time _elapsed) {

    if (_garrazoActivo) {
        tiempoAcumuladoGarrazo += _elapsed;
        if (alpha > 10) alpha = alpha - 4;
        if (aleatorio == 0)_spriteGarrazo->setColor(sf::Color(255, 255, 255, alpha));
        else _spriteGarrazo2->setColor(sf::Color(255, 255, 255, alpha));

        if (tiempoAcumuladoGarrazo.asSeconds() > 1) {
            //std::cout << "GARRAZO EFECTO" << std::endl;
            _garrazoActivo = false;
            _spriteGarrazo->setColor(sf::Color(255, 255, 255, 0));
            _spriteGarrazo2->setColor(sf::Color(255, 255, 255, 0));
            tiempoAcumuladoGarrazo = sf::seconds(0.f);
        }
    }
}

//********************************************************************************************
//DIBUJA EL GARRAZO
//utiliza la boleana _gararzoActivo para saber cuando dibujar
//tambien verifica  un numero aleatorio y segun el caso (cero o uno) dibuja un sprite u otro
//********************************************************************************************

void Garrazo::Dibujar(sf::RenderWindow* _window) {

    if (_garrazoActivo)
        switch (aleatorio) {
            case 0:
                _window->draw(*_spriteGarrazo);
                break;
            case 1:
                _window->draw(*_spriteGarrazo2);
                break;
        }
}
