/*
 * CLASE PERSONAJES
 * esta clase gestiona a los personajes, sirve para los rehenes y los enemigos:
 * rehenes: abuela y abuelo....... 
 * enemigos: dracula y lobo
 * setea textura, la posicion a los sprites de los personajes
 * 
 */

/* 
 * File:   Personajes.h
 * Author: Maria Emilia Corbetta
 */

#ifndef PERSONAJES_H
#define PERSONAJES_H

//incluimos las librerias necesarias
#include <SFML/Graphics.hpp>
#include <iostream>

class Personajes {
public:
    //constructor y destructor
    Personajes();
    ~Personajes();
    
    //METODOS PUBLICOS
    void Actualizar(sf::Time _elapsed);
    void Dibujar(sf::RenderWindow* _window);
    void SetPosicionPersonajes(int posicion);
    sf::FloatRect GetSpriteBounds();
    bool GetEsEnemigo();
    bool EstaActivo();
    void SetActivarPersonaje();
    void SetDesactivarPersonaje();
    bool EstaAtacando();
    
private:
    //ATRIBUTOS PRIVADOS
    sf::Texture* _texturaDracula;
    sf::Sprite* _spriteDracula;

    sf::Texture* _texturaLobo;
    sf::Sprite* _spriteLobo;

    sf::Texture* _texturaAbuela;
    sf::Sprite* _spriteAbuela;

    sf::Texture* _texturaAbuelo;
    sf::Sprite* _spriteAbuelo;

    int personajeActivo;
    bool _activarPersonaje;
    bool _estaAtacando;
    bool _atacoUnaVez;
    
    sf::Time _tiempoAcumulado;
    
};

#endif /* PERSONAJES_H */

