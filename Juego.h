/*CLASE JUEGO
 * esta es la clase principal del nuestro juego
 * tiene el metodo IniciarJuego() que es bucle principal del juego(unico metodo publico que se llama desde el main.cpp)
 * se procesa eventos
 * sea actualiza la logica del juego (cuando pierde,cuando se detecta un dipsaro, la logica de las aberturas,cuando hay efectos)
 * se detectan los disparos(colisione entre cursor y rectangulo del sprite de los personajes) y ataques(enemigo o rehen?)
 * aca se instancia el objeto reloj
 * tiene el metodo Dibujar para dibujar todos los elementos del juego
 * 
 */

/* 
 * File:   Juego.h
 * Autora: Maria Emilia Corbetta
 */

#ifndef JUEGO_H
#define JUEGO_H

#include <SFML/Graphics.hpp>
#include <iostream>

//incluimos los headers de las clases que necesitamos usar aca
#include "Escenario.h"
#include "Ventanas.h"
#include "Personajes.h"
#include "Puerta.h"
#include "Player.h"
#include "Aberturas.h"
#include "Ui.h"
#include "GameOver.h"
#include "Efectos.h"
#include "Sonido.h"

class Juego {
public:
    //CONSTRUCTOR Y DESTRUCTOR DE LA CLASE
    Juego();
    ~Juego();

    //METODO PUBLICO
    void IniciarJuego();

private:
    //ATRIBUTOS PRIVADOS
    sf::RenderWindow* _window;

    //objetos
    Escenario* _escenario;
    Personajes* _personajesVentArr[5];
    Personajes* _personajePuerta;
    Aberturas* _ventanasArr[5];
    Aberturas* _puerta;
    Player* _playerEmily;
    Ui* _ui;
    GameOver* _gameOver;
    Efectos* _efectos;
    Sonido* _sonido;

    //variables para medir el tiempo en metodo actualizar
    sf::Clock _reloj;
    sf::Time _elapsed;

    //boleanas 
    bool _estaDisparando;
    bool _juegoPerdido;

    //METODOS PRIVADOS
    void ProcesarEventos();
    void Actualizar();
    void Dibujar(sf::RenderWindow* _window);
    void GestionPersonajes(Personajes* personaje, Aberturas* abertura);
    void DetectarDisparo(Personajes* personaje, Aberturas* abertura);
    void DetectarAtaque(Personajes* personaje, Aberturas* abertura);
};

#endif /* JUEGO_H */

