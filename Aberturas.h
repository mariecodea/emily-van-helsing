/*
 * CLASE MADRE: esta es la clase madre de puerta y ventanas
 * sirve para que de ella se puedan heredar atributos y metodos para:
 * medir el tiempo
 * aleatorizar abierto y cerrado de puerta y ventana
 * cambiar el estado 
 * posicionar puerta y ventana
 * Me sirve para poder utilizar polimorfismo
 * 
 */

/* 
 * File:   Aberturas.h
 * Autora: Maria Emilia Corbetta
 */

#ifndef ABERTURAS_H
#define ABERTURAS_H

//incluimos las librerias necesarias
#include <SFML/Graphics.hpp>
#include <iostream>

class Aberturas {
public:
    //constructor y destructor de la clase
    Aberturas();
    ~Aberturas();

    //atributos publicos
    bool _estaCerrada;
    float tiempoDeIntervalo;
    sf::Time _tiempoAcumulado;

    //metodos publicos
    void CambiarEstado();
    bool GetEstaCerrada();
    void Actualizar(sf::Time _elapsed);

    //los siguientes metodos son virtuales y cada hija podra redefinirlos 
    virtual void SetPosicionAberturas(int idAbertura) = 0;
    virtual void Dibujar(sf::RenderWindow* _window) = 0;

private:

};

#endif /* ABERTURAS_H */

